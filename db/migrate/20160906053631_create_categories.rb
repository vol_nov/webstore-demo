class CreateCategories < ActiveRecord::Migration[5.0]
  def self.up
    create_table :categories do |t|
      t.string :name
      t.timestamps
    end

    Category.create :name => "Phones"
    Category.create :name => "Notebooks"
    Category.create :name => "Pcs"
    Category.create :name => "E-books"
    Category.create :name => "Cameras"
  end
end
