Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'index#index'

  get 'index/about'
  get 'index/help'
  get 'home', to: "product#index"
  get 'products', to: "product#index"
  get 'products/add', to: 'product#add'
  get 'products/all', to: "product#all"
  get 'products/bought', to: "product#bought"
  get 'login', to: "user#login"
  get 'register', to: "user#register"
  get 'logout', to: "user#logout"


  post 'user/create'
  post 'user/validate'
  post 'product/addproduct'
  post 'product/buy'

  resources :index
  resources :register
  resources :login
  resources :home
end
