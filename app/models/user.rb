class User < ApplicationRecord
  validates :password, presence: true
  validates :email, presence: true
  validates :phone, presence: true
  validates :fullname, presence: true
end
