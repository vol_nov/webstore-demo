class Product < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_many :requests
  validates :name, presence: true
end
