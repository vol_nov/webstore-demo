class UserController < ApplicationController
  def register
    @user = User.new
  end

  def login
    @user = User.new
  end

  def validate
    @foundUser = User.find_by(email: user_params[:email], password: user_params[:password])

    if @foundUser
      session[:user_id] = @foundUser.id
      redirect_to :action => 'index', :controller => 'product'
    else
  end

  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to :action => 'index', :controller => 'login'
    else
      redirect_to :action => 'index', :controller => 'register'
    end
  end

  def logout
    session.delete(:user_id);
    redirect_to :action => "login", :controller => "user"
  end
  

  private
    def user_params
      params.require(:user).permit(:password, :email, :phone, :fullname)
    end
end
