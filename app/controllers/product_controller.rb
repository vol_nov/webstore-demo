class ProductController < ApplicationController
	before_action :logged_in?, only: [:index, :all, :bought, :add]
  def index
      @products = Product.where(user_id: session[:user_id]) 
  end
    
  def all
	  @products = Product.where('user_id <> ? AND bought = ?', session[:user_id] , false)
	  @request = Request.new  
  end

  def buy 
    @request = Request.new(req_params)
    @prod = Product.where(id: @request.product_id)
    @prod.bought = true
    @prod.save
    @request.save

    redirect_to :action => "all", :controller => "product"
  end

  def bought
    @requests = Request.where(user_id: session[:user_id])
    @products = []

    @requests.each do |r|
      @products.push(Product.where(id: r.product_id).take)
    end

    #@products = Product.all.select { |p| ( }).
    #  any? { |r| r.product_id == p.id} }
  end

  def add
     @product = Product.new
     @categories = Category.all.collect{ |c| [c.name, c.id] }
  end

  def addproduct
    @product = Product.new(prod_params)
    
    if @product.save
      redirect_to :action => 'index', :controller => 'product'
    else
      redirect_to :action => 'login', :controller => 'user'
    end

  end

  private 
    def prod_params
      params.require(:product).permit(:name, :description, :price, :category_id, :user_id)
    end

    def req_params
      params.require(:request).permit(:product_id, :user_id)
    end

    def logged_in? 
      puts "User_id: " + session[:user_id].to_s
      unless session.key?(:user_id)
        redirect_to :action => "login", :controller => "user"
      end
    end
end
